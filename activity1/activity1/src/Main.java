import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner yearScanner = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year.");

        int checkYear = yearScanner.nextInt();

        if ((checkYear % 4 == 0 && checkYear % 100 != 0) || (checkYear % 400 == 0)) {
            System.out.println(checkYear + " is a leap year.");
        } else {
            System.out.println(checkYear + " is NOT a leap year.");
        }
    }
}
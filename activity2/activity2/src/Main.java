import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        // Array
        int[] primeNumbers = new int[5];

        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        System.out.println("The first prime number is: " + primeNumbers[0]);
        System.out.println("The second prime number is: " + primeNumbers[1]);
        System.out.println("The third prime number is: " + primeNumbers[2]);
        System.out.println("The fourth prime number is: " + primeNumbers[3]);
        System.out.println("The fifth prime number is: " + primeNumbers[4]);


        // ArrayList
        ArrayList<String> friends = new ArrayList<String>(Arrays.asList("Kayden", "Kartein", "Cain", "Schnauder"));

        System.out.println("My friends are: " + friends);


        // HashMap
        HashMap<String, Integer> triggers = new HashMap<String, Integer>(){
            {
                put ("Kogetsu", 8);
                put ("Scorpion", 6);
                put ("Raygust", 10);
            }
        };

        System.out.println("Trigger durability on a scale of 1 to 10: " + triggers);
    }
}